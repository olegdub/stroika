# README

Здесь мы формируем наши стандарты для вёрстки.

- base-blocks - заготовки для блоков страниц и стилей.
- base-template - заготовка для проекта вёрстки. Основано на [этом](https://github.com/Harrix/static-site-webpack-habr).
- grandcore.org - текущая вёрстка нашего проекта.

## Для запуска
- cd (путь для репозитория)/grandcore.org
- npm install
- npm run start
## INIT
1) Сделать Fork проекта в свой репозиторий
https://gitlab.com/grandcore/stroika
2) Склонировать СВОЙ репозиторий
$ git clone git@gitlab.com:NAME/stroika.git
3) Подключить свой репозиторий к удаленному для закрузки изменений
$ git remote add upstream git@gitlab.com:grandcore/stroika.git

## USING
1) Пилим фичи
2) $ git add .
3) $ git commit -m 'топовая фича'
4) пушим на свой репозиторий
$ git push
5) если всё ОК, заходим в гитлаб и в merge requests и кидаем реквест на ветку master (пока что)
6) ждем подтверждения

и перед началом работы стоит завести привычку подгружаться
$ git pull
так же не будет лишним писать в отдельных ветках. но с этим если, что по ходу дела разберемся

## Полезные ссылки
- [Макеты в Figma](https://www.figma.com/file/I4qnzmeIwHHPRu5RACGvwU/GrandCore-%D0%92%D1%91%D1%80%D1%81%D1%82%D0%BA%D0%B0?node-id=0%3A1)
- [Текущие задачи в Trello](https://trello.com/b/6niwvQ5a/frontend)
- [Пишите в VK](https://vk.com/grandcore) кого добавить в рабочуу группу
- [Пишите в Telegram](https://t.me/grandcore) куратору проекта